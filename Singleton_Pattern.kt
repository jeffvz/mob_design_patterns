class Database {
    companion object {
        private var instance:Database ? = null
        const val instanceName = "couchdb"
        fun getInstance():Database{
            if (instance == null){
            	instance = Database()    
            }
            return instance!!
        }
    }
}

fun main()
{
    val databaseInstance = Database.getInstance()
    print(databaseInstance)
}