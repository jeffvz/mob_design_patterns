interface ConsoleConnector{
    fun update(section:Int, sectionName:String, state:Boolean)
}

class MixingConsole() {
    var connectors: MutableList<ConsoleConnector> = ArrayList()

    fun plug(connector:ConsoleConnector){
        this.connectors.add(connector)
        println("New device plugged")
    }

    fun unplug(connector:ConsoleConnector){
        this.connectors.remove(connector)
        println("Device unplugged")
    }

    fun setUpDevice(section:Int, sectionName:String, state:Boolean){
        for (connector in this.connectors){
            connector.update(section, sectionName, state)
        }
    }

}

class Microphone : ConsoleConnector{
    override fun update(section:Int, sectionName:String, state:Boolean){
        println("Microphones have been plugged in secction: ${section} -> ${sectionName}")
    }
}

class Screen : ConsoleConnector{
    override fun update(section:Int, sectionName:String, state:Boolean){
        println("Screen have been plugged in secction: ${section} -> ${sectionName}")
    }
}


fun main(){
    var mixingConsole = MixingConsole()
    
    var mixcrophoneInstance = Microphone()
    var screenInstance = Screen()

    mixingConsole.plug(mixcrophoneInstance)
    mixingConsole.plug(screenInstance)

    mixingConsole.setUpDevice(1, "Section Microphones-Screens", true)
    mixingConsole.unplug(mixcrophoneInstance)
}

/*
PRINTS:
    New device plugged
    New device plugged
    Microphones have been plugged in secction: 1 -> Section Microphones-Screens
    Screen have been plugged in secction: 1 -> Section Microphones-Screens
    Device unplugged
*/