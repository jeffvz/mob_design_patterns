interface Car {
    var type:String
    var weight:Double

    fun run(speed:Double)
    fun stop()
}

class CarFactory{
    fun makeSedan():Car{
        return Sedan()
    }
    fun makeTruck():Car{
        return Truck()
    }
}

class Sedan():Car{

    override var type: String = "Sedan"
    override var weight: Double = 1000.00

    override fun run(speed: Double) {
	println("I'm a ${type} and I'm running at ${speed} km/h")
    }
    override fun stop() {
	println("I'm stopping")
    }
}

class Truck():Car{
    override var type: String = "Truck"
    override var weight: Double = 4000.00

    override fun run(speed: Double) {
	println("I'm a ${type} and I'm running at ${speed} km/h")
    }
    override fun stop() {
	println("I'm stopping")
    }
}


fun main(args : Array<String>){
    val carFactory = CarFactory()

    val sedan = carFactory.makeSedan()
    val truck = carFactory.makeTruck()

    println(truck.type)
    println(sedan.type)

    sedan.run(40.0)
    sedan.stop()

    /*
      prints:
      	Truck
	    Sedan
	    I'm a Sedan and I'm running at 40.0 km/h
	    I'm stopping
    */
}