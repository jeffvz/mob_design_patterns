interface Builder
{
    fun buildComputerMainboard()
    fun buildComputerProcessor()
    fun buildComputerRAM()
    fun buildComputerDisk()
}

class Computer
{
    private var parts = arrayListOf<String>()

    fun assembly(part:String){
        (this).parts.add(part)
    }

    fun assembledComputer():String{
        return "Computer: ${parts.joinToString(separator="-")} \n"
    }
}

class ComputerBuilder:Builder
{
    private var computer = Computer()

    override fun buildComputerMainboard(){
        computer.assembly("Intel")
    }
    override fun buildComputerProcessor(){
        computer.assembly("i8 8550x")
    }
    override fun buildComputerRAM(){
        computer.assembly("16GB")
    }
    override fun buildComputerDisk(){
        computer.assembly("1TB SSD")
    }

    fun reset(){
        computer = Computer()
    }

    fun getComputer():Computer{
        val computer = this.computer
        reset()
        return computer
    }
}

class ComputerAssemblyDirector
{
    private var computerBuilder : ComputerBuilder

    constructor(computerBuilder: ComputerBuilder){
        this.computerBuilder = computerBuilder
    }

    fun getComputer():Computer{
        return this.computerBuilder.getComputer()
    }

    fun makeAComputer(){
        this.computerBuilder.buildComputerMainboard()
        this.computerBuilder.buildComputerProcessor()
        this.computerBuilder.buildComputerRAM()
        this.computerBuilder.buildComputerDisk()
    }

    fun makeAComputerWithoutDisk(){
        this.computerBuilder.buildComputerMainboard()
        this.computerBuilder.buildComputerProcessor()
        this.computerBuilder.buildComputerRAM()
    }

}

fun main(args : Array<String>){
    val computerBuilder = ComputerBuilder()
    val computerDirector = ComputerAssemblyDirector(computerBuilder)

    computerDirector.makeAComputer()
    val myComputer = computerDirector.getComputer()

    computerDirector.makeAComputerWithoutDisk()
    val myComputerWithoutDisk = computerDirector.getComputer()

    println(myComputer.assembledComputer())
    println(myComputerWithoutDisk.assembledComputer())

    /*
    prints:
        Computer: Intel-i8 8550x-16GB-1TB SSD

        Computer: Intel-i8 8550x-16GB
     */
}
