interface PizzaBase{
    fun getPrice():Double
    fun getName():String
}

class PizzaGourmet: PizzaBase{
    private var gourmetPrice = 2.3
    private var name:String = "Gourmet"
    
    override fun getPrice():Double{
        return this.gourmetPrice
    }
    override fun getName():String{
        return "Pizza "+this.name
    }
}

abstract class PizzaDecorator: PizzaBase{
    private var decoratedSource: PizzaBase? = null
    
    constructor(decoratedSource:PizzaBase){
        this.decoratedSource = decoratedSource
    }
    
    abstract override fun getPrice():Double
    abstract override fun getName():String
}

class ExtraCheese:PizzaDecorator {
    private var decoratedPizza: PizzaBase? = null
    
    constructor(decoratedPizza:PizzaBase):super(decoratedPizza){
        this.decoratedPizza = decoratedPizza
    }
    override fun getPrice():Double{
        return decoratedPizza!!.getPrice() + 1.50
    }
    override fun getName():String{
        return decoratedPizza!!.getName() + " with extra cheese"
    }
}

class ExtraMushrooms:PizzaDecorator {
    private var decoratedPizza: PizzaBase? = null
    
    constructor(decoratedPizza:PizzaBase):super(decoratedPizza){
        this.decoratedPizza = decoratedPizza
    }
    override fun getPrice():Double{
        return decoratedPizza!!.getPrice() + 0.70
    }
    override fun getName():String{
        return decoratedPizza!!.getName() + " with extra mushrooms"
    }
}

fun main(args : Array<String>){
    var pizzaWithExtraCheese:PizzaBase = ExtraCheese(PizzaGourmet())
    println(pizzaWithExtraCheese.getName())
    println(pizzaWithExtraCheese.getPrice())
}

/*
 PRINTS:
* Pizza Gourmet with extra cheese
* 3.0

*/