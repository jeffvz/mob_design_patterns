interface Guitar{
    fun isOff():Boolean
    fun powerOn():String
    fun powerOff():String
    var volume:Int
    var effect:String
}

class GuitarPedal
{
    private var guitar:Guitar

    constructor(guitar:Guitar){
        this.guitar = guitar
    }

    fun togglePower():String{
        if (this.guitar.isOff()){
            return this.guitar.powerOn()
        }else{
            return this.guitar.powerOff()
        }
    }

    fun volumeDown() {
        this.guitar.volume = this.guitar.volume - 5
    }
    fun volumeUp(){
        this.guitar.volume = this.guitar.volume + 5
    }

    fun switchEffect(efect:String){
        this.guitar.effect = efect
    }
}

class ElectricGuitar:Guitar{

    override var volume: Int = 0
    override var effect = ""

    override fun isOff():Boolean{
        return true
    }
    override fun powerOn():String{
        return "Turned on"
    }
    override fun powerOff():String{
        return "Turned off"
    }

}

class ClassicGuitar:Guitar{

    override var volume: Int = 2
    override var effect = "Clean"

    override fun isOff():Boolean{
        return true
    }
    override fun powerOn():String{
        return "Turned on"
    }
    override fun powerOff():String{
        return "Turned off"
    }

}

fun main(args : Array<String>){
    val electricGuitar = ElectricGuitar()
    val guitarPedal = GuitarPedal(electricGuitar)

    println(guitarPedal.togglePower())
    guitarPedal.switchEffect("Distortion")

    guitarPedal.volumeUp()
    guitarPedal.volumeUp()
    guitarPedal.volumeUp()

    println(electricGuitar.volume)
    println(electricGuitar.effect)

    /*
    prints:
        Turned on
        15
        Distortion
     */
}